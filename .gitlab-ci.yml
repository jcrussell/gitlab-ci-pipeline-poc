image: docker:latest

services:
- docker:dind

stages:
- Build test Docker image
- Test and push

variables:
  APP_TAG_NAME: registry.gitlab.com/jcrussell/gitlab-ci-pipeline-poc/my_app:$CI_COMMIT_REF_NAME
  DOCKER_DRIVER: overlay2

before_script:
  # docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
- docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com

docker:build:
  stage: Build test Docker image
  # Cache vs. artifacts, in case you're wondering:
  # https://docs.gitlab.com/ee/ci/caching/
  # TLDR Caching is not designed to pass artifacts between stages.
  # Cache is for runtime dependencies needed to compile the project.
  artifacts:
    paths:
    - docker-images
  script:
  - mkdir docker-images
  - docker pull $APP_TAG_NAME || true # This is not the same as the --pull argument for docker build, which will try to pull the newer base image (whatever is in FROM), NOT to update this very image from the registry.
  - docker build --cache-from $APP_TAG_NAME -f Dockerfile.dev -t $APP_TAG_NAME .
  - docker save $APP_TAG_NAME > docker-images/image.tar

# All these tests run in every case, even if no code related to them
# have been changed. For instance if you just changed some controllers,
# there is no need for the jest tests to run.
#
# Since the speed really is important in this, it'd be worth checking
# in Git history whether there are new changes in given area from the
# last build.
#
# This is however out of the scope of this little proof of concept.
rspec:
  stage: Test and push
  script:
  - docker load -i docker-images/image.tar
  - docker run --rm $APP_TAG_NAME bundle exec rspec

rubocop:
  stage: Test and push
  script:
  - docker load -i docker-images/image.tar
  # NOTE: this command will always succeed.
  # I don't want to get too deep into it, it's just an example.
  #
  # I want the example to be realistic, that's why there are
  # multiple jobs, but we don't really care what rubocop says.
  - docker run --rm $APP_TAG_NAME bundle exec rubocop --lint

jest:
  stage: Test and push
  script:
  - docker load -i docker-images/image.tar
  - docker run --rm -w /app/frontend -e CI=true $APP_TAG_NAME yarn test src

# Despite the fact that push should logically be a separate stage,
# I decided against that.
#
# For one thing we don't want to wait until all the tests, including
# the slow Cypress tests finish.
#
# And more importantly we always want to push the image, even should
# the tests fail. Keep in mind, this is not the production Docker image,
# it's for CI use only. So if we update Gemfile, push the code and
# the build fails, we still want to cache that build, because in
# the next commit we're unlikely to touch Gemfile itself, we're more
# likely just to fix a controller or spec file.
#
# Therefore merging these two logical steps into one gives use better
# caching and faster builds.
#
# Also as the push happens sooner and you commit in a rather quick
# succession, the new commit is more likely to get the already-rebuilt
# image as the image was pushed say in minute 8 of the build rather
# than 14.
#
# TODO: Why does it take so long (4:38)? It should be done in an instant
# since we only have to push the new / changed layers that are not in
# the registry yet.
#
# We might try a different registry, but pushing it into this common
# stage, the issue is not as big, since we're going to wait for the
# tests to finish anyway.
docker:push:
  stage: Test and push
  script:
    - docker load -i docker-images/image.tar
    - docker push $APP_TAG_NAME
  only:
    - master

cypress:
  stage: Test and push
  script:
    - docker load -i docker-images/image.tar
    - docker run --rm --name server -d $APP_TAG_NAME
    - docker run --rm --link server -w /app/frontend $APP_TAG_NAME sh -c 'yarn wait-on http://server:3000 && yarn cypress'
