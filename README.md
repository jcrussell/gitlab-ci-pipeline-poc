# About

This is a proof of concept of an alternative approach to GitLab CI pipelines I'm trying in order to speed up running of especially Cypress tests.

# Notes

- `Dockefile.dev` is for the CI, it's not the deployment `Dockerfile` (which is missing since we don't need it here in this little proof of concept).
