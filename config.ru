require 'sinatra'

# Always return the main SPA file.
not_found do
  status 200
  File.read('frontend/build/index.html')
end

get '/api/v1/tasks' do
  content_type :json
  [
    {
      title: 'First task'
    },
    {
      title: 'Second task'
    }
  ].to_json
end

run Sinatra::Application
