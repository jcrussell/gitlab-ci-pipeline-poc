require 'spec_helper'

describe "Serving the SPA app" do
  it "returns the main SPA file" do
    get '/'
    expect(last_response).to be_ok
    expect(last_response.content_type).to eql('text/html;charset=utf-8')
  end
end

describe "API" do
  describe 'GET /api/v1/tasks' do
    it "lists existing tasks" do
      get '/api/v1/tasks'
      expect(last_response).to be_ok
      expect(last_response.content_type).to eql('application/json')

      body = JSON.parse(last_response.body)
      expect(body.length).to eql(2)
    end
  end
end
