require 'rack/test'
require 'rspec'

ENV['RACK_ENV'] = 'test'

sinatra_app = File.read(File.expand_path('../../config.ru', __FILE__))
builder = Rack::Builder.new
builder.instance_eval(sinatra_app)

module RSpecMixin
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end
end

RSpec.configure { |c| c.include RSpecMixin }
